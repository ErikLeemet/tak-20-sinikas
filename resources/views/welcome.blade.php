<x-guest-layout>
<div class="bg-gray-700 dark:bg-gray-900">
            @if (Route::has('login'))
                <div class="flex justify-center top-5 right-0 px-6 py-4 lg:block">
                    @auth
                        <a href="{{route('cart')}}">{{count($cart)}}</a>
                        <a href="{{ url('/dashboard') }}" class="text-sm text-gray-50 dark:text-gray-500 underline">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-50 dark:text-gray-500 underline">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-50 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        <div class="flex flex-col md:grid md:grid-cols-2 lg:grid-cols-3 bg-gray-900">
            @foreach ($products as $product)
            <form action="{{route('addcart')}}" method="post">
                @csrf
            <input type="hidden" name="id" value="{{$product->id}}">
            <div class="mx-10 my-6 p-4 bg-gray-800 text-white shadow-lg rounded-lg">
                <div class="h-56">
                    <img src="{{ $product->image }}" alt="image" class="object-fill h-full w-full rounded-xl">
                </div>
                <div class="flex justify-between items-center py-6">
                    <p class="text-2xl font-bold">{{ $product->name }}</p>
                    <span class="rounded-lg bg-blue-700 text-white p-2">{{ $product->price }} €</span>
                </div>
                <span> Description: </span>
                <div class="text-gray-400 my-4 flex justify-center">{{ $product->discription }}</div>
                <div class="text-center">
                    <button type="submit" class="rounded-lg text-white cursor-pointer border-dotted border-2 bg-blue-700 text-xl font-bold py-2 px-14">Add to cart</button>
                </div>
            </div>
        </form>
        @endforeach
    </div>
</x-guest-layout>
