<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight"></h2>
        {{__('Store') }}
    </x-slot>
    <div class="">
        <a class="rounded px-2 py-1 bg-gray-800 text-white" href="{{route('products.add')}}">Add new product</a>
    </div>
</x-app-layout>
