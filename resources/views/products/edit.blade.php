<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight"></h2>
        {{__('editProducts') }}
    </x-slot>
    <div class="max-w-screen-sm mx-auto mt-16">
        <form action="{{route('update')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="id" value="{{$product->id}}">
            <x-label for="name" :value="__('Name')" />
            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name') ?? $product->name" required autofocus />

            <x-label for="discription" :value="__('Description')" />
            <x-input id="discription" class="block mt-1 w-full" type="text" name="discription" :value="old('discription') ?? $product->discription" required autofocus />

            <x-label for="price" :value="__('Price')" />
            <x-input id="price" class="block mt-1 w-full" type="text" name="price" :value=" old('price') ?? $product->price" required autofocus />

            <x-label for="image">Add image</x-label>
            <input id="image" class="mt-1" type="file" name="image"/>

            <button class="px-2 py-1" type="submit">Submit</button>
        </form>
    </div>
</x-app-layout>
