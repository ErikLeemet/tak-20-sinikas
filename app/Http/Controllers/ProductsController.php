<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products', [
            'products' => Products::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('addProducts');
    }

    public function edit($id)
    {
       return view('products/edit', [
        'product' => Products::find($id)
    ]);
    }

    public function update(Request $request)
    {
        $product = Products::find($request->id);
        $request->validate([
            'name' => 'required',
            'discription' => 'required',
            'price' => 'required',
        ]);
        $path = $product->image;
        $file = $request->file('image');

        if(file_exists($file)){
            Storage::delete($product->image);
            $path = Storage::putFile('images', $file);
        };

        $product->update([
            'name' => $request->name,
            'discription' => $request->discription,
            'price' => $request->price,
            'image' => $path,
        ]);
        return redirect()->route('products')
                        ->with('success','Product created successfully.');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $products = Products::find($request->id);
        $products->delete();
        Storage::delete($products->image);

        return redirect()->route('products')
                        ->with('success','Product deleted successfully.');
    }
    public function store(Request $request)

    {
        $request->validate(['name' => 'required','discription' => 'required','price' => 'required',]);

$path = Storage::putFile('images', $request->file('image') );
    Products::create([
        'name' => $request->name,
        'discription' => $request->discription,
        'price' => $request->price,
        'image' => $path
        ]);
        return redirect()->route('products')
                        ->with('success','Product created successfully.');
    }
    public function show(Products $products)

    {

        return view('addProducts',compact('products'));

    }

}
