<?php

namespace App\Http\Controllers;

use App\Mail\Paymentcomplete;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class StoreController extends Controller

{
    public function index()
    {
        return view('welcome' , [
            'products' => Products::all(),
            'cart' => session('cart', [])
            ]);
    }
    public function addtocart(Request $request)
    {
        $cart = $request->session()->get('cart');

        $quantity = 0;
        if($request->qty <= 0){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }

        $product = Products::find($request->id);
        $productitem = [
            'id' => $product->id,
            'name' => $product->name,
            'description' => $product->discription,
            'price' => $product->price,
            'image' => $product->image,
            'quantity' => $quantity
        ];
        if (!$cart) {
            $cart = [
                $product->id => $productitem
            ];
            $request->session()->put('cart', $cart);
            return redirect()->back();
        }
        if (isset($cart[$product->id])) {
            $cart[$product->id]['quantity']++;
            $request->session()->put('cart', $cart);
            return redirect()->back();
        }
        $cart[$product->id] = $productitem;
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }

    public function cart(){
        $cart = collect(session('cart'));
        $total = $cart->map(function ($item) {
            return intval(($item['price']) * $item['quantity']);
        });
        return view('cart', [
            'cart' => session('cart', []),
            'total' => $total->sum()
        ]);
    }
    public function updatecart(Request $request){
        $quantity = 0;
        if($request->quantity <= 0){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }
        $cart = session('cart');
        $cart[$request->id]['quantity'] = $quantity;
        $request->session()->put('cart', $cart);

        return redirect()->back();
    }
    public function deleteitem(Request $request){
        $cart = session('cart');
        unset($cart[$request->id]);
        $request->session()->put('cart', $cart);
        return redirect()->back();
    }
    public function subscribe()
    {
        $cart = collect(session('cart'));
        $total = $cart->map(function ($item) {
            return intval(($item['price'] * 100) * $item['quantity']);
        });
        Stripe::setApiKey('sk_test_51KH69OAPEsKMRoTM5t7lMrQjfkuM3hB6U6u9wd12VjgmfdAjiQ60iMbzzgt0NyaBWuVEsthABD1jF7tFzQGT61nw00GNwPxv4c');
        $paymentIntent = PaymentIntent::create([
            'amount' => $total->sum(),
            'currency' => 'eur',
            'payment_method_types' => [
                'card'
            ],
        ]);
        $output = [
            'paymentIntent' => $paymentIntent,
            'stripePublicKey' => "pk_test_51KH69OAPEsKMRoTMVT6PWu6uUCAMUs2GrtVzSfrUiyi9WGfyIeHs0ZLqLoA8h68eCAQU8A7eU0ATiV8jlqCraOpo00Zp8WkqoV"
        ];
        return $output;
    }
    public function success()
    {
        Mail::to("test@test.com")->send(new Paymentcomplete(session('cart')));
        session()->forget('cart');
        return;
    }
}
